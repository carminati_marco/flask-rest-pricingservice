import unittest
import os
import json
import api
from datetime import date, timedelta

class PricingServiceTestCase(unittest.TestCase):
    """This class represents test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        api.app.testing = True
        self.client = api.app.test_client

    def test_api_root_post(self):
        """Test API root"""        
        print 'TEST Simple'
        data = {"order": { "id": 12345, "customer": "Something" , 
        "items": [{"product_id": 1, "quantity": 1 }, 
        {"product_id": 2, "quantity": 5 }, 
        {"product_id": 3, "quantity": 1 } ]}}
        
        res = self.client().post('/', headers={'Content-Type': 'application/json'}, 
        data=json.dumps(data))
        data = json.loads(res.get_data())
        print data
        self.assertEqual(res.status_code, 200)
        self.assertEqual(2099.00, data['total_price'])
        self.assertEqual(119.8, data['total_vat'])
        
    
    
    def test_api_root_post_EUR(self):
        """Test API root"""
        print 'TEST Currency EUR'
        data = {"currency": "EUR", "order": { "id": 12345, "customer": "Something" , 
        "items": [{"product_id": 1, "quantity": 1 }, 
        {"product_id": 2, "quantity": 5 }, 
        {"product_id": 3, "quantity": 1 } ]}}
        
        res = self.client().post('/', headers={'Content-Type': 'application/json'}, 
        data=json.dumps(data))
        data = json.loads(res.get_data())
        self.assertEqual(res.status_code, 200)
        
        print "THE VALUES CAN CHANGE!!! SEE https://api.fixer.io/latest?base=GBP"
        print data
        self.assertEqual(2348.57, data['total_price'])
        self.assertEqual(134.04, data['total_vat'])  

if __name__ == "__main__":
    unittest.main()
