from datetime import datetime
import json, urllib
import os
import requests

from flask import Flask, request, url_for
from flask_restful import Resource, Api, reqparse

from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()

app = Flask(__name__)
api = Api(app)

def _get_rates():
    rates = cache.get("rates")
    if rates is None:
        url = "https://api.fixer.io/latest?base=GBP"
        response = urllib.urlopen(url)
        rates = json.loads(response.read())
        cache.set("rates", rates, timeout=5 * 60)    
    return rates

def _get_pricing():
    pricing = cache.get("pricing")
    if pricing is None:
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
        json_url = os.path.join(SITE_ROOT, "pricing.json")
        pricing = json.load(open(json_url))
        cache.set("pricing", pricing, timeout=5 * 60)
    return pricing    

PRICING = _get_pricing()
RATES = _get_rates()

def _get_rate(currency):
    """ with the currency code, it obtains the realtive rage.
    """
    if currency in RATES["rates"]:
        return RATES["rates"][currency]
    else:
        return 0 #todo: exception class

def _get_item_price_VAT(item, rate):
    """ with the item and rate, it obtains price, vat, total_price (price+vat)
    """
    
    for i in PRICING["prices"]:
        # check if i found the relative prices.
        if i["product_id"] == item["product_id"]:
            single_price = i["price"]
            vat_band = i["vat_band"]
            pass
    
    # cast to 2 decimals using format float.
    price = float("%.2f" % (rate * item["quantity"] * single_price))
    vat = float("%.2f" % (price * PRICING["vat_bands"][vat_band]))
    # calculate the total value.
    total_price = price + vat 
    return price, vat, total_price


def _get_response(request_items, rate):
    """ with request_items and rate, it obtains the total price for the order, 
    the total VAT for the order, 
    price and VAT for each item in the order
    """
    items = []
    total_price, total_vat = 0, 0
    
    for request_item in request_items:
        # obtains data of single item.
        item_price, item_vat, item_total_price = _get_item_price_VAT(request_item, rate)        

        items.append(dict(price=item_price, vat=item_vat, total_price=item_total_price))        
        total_vat += item_vat
        total_price += item_price
    
    # create response.
    response = dict(items=items, total_price=total_price, total_vat=total_vat)
    return response


class BaseAPI(Resource):
    def get(self):
        return {"hello": "it works!"}

    def post(self):   
        json_data = request.get_json()
        # calculate rate if currency different (use 1 for identical currency).
        rate = _get_rate(json_data["currency"]) if "currency" in json_data else 1
        response = _get_response(json_data["order"]["items"], rate)
        return response
    
    
api.add_resource(BaseAPI, "/")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
    